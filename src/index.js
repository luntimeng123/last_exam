import React from 'react'
import ReactROM from 'react-dom'
import App from './App'
import { Provider } from 'react-redux'
import { store } from './redux/store/store'

ReactROM.render(

    <Provider store={store}>
        <App />
    </Provider>,

    document.getElementById('root')

)