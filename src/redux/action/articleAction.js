import { fetch_all_article } from "../../service/article.service"

export const FETCH_ALL_ARTICLES = "FETCH_ALL_ARTICLES"



//****fetch all article from api ***/

export const fetchAllArticle = () => {

    return async (dispatch) => {

        const result = await fetch_all_article()

        dispatch({
            type : FETCH_ALL_ARTICLES,
            payload : result
        })

    }

}