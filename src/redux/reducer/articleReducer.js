import { FETCH_ALL_ARTICLES } from "../action/articleAction"



const initStete = {
    articles : []
}



export default function articleReducer(state = initStete,action){

    switch (action.type) {

        case FETCH_ALL_ARTICLES :
            
            return {
                ...state,
                articles : action.payload
            }

        default:

            return state
    }
}