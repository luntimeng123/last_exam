import { applyMiddleware, combineReducers, createStore } from "redux";
import thunk from 'redux-thunk'
import articleReducer from "../reducer/articleReducer";


//combind reducer
const rootReducer = combineReducers({
    articleReducer,
})


export const store = createStore(rootReducer,applyMiddleware(thunk))