import { api } from './api/api'



// ***** fetch all article --->

export const fetch_all_article = async () => {

    try {
        
        const result = await api.get('/articles')

        return result.data.data

    } catch (error) {
        console.log("error_fetch_all_article",error)
    }

}


// ***** fetch a article by id--->