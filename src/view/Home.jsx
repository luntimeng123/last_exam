import React from 'react'
import { useEffect } from 'react'
import { Card, Button } from 'react-bootstrap'
import { useDispatch, useSelector } from 'react-redux'
import { fetchAllArticle } from '../redux/action/articleAction'


export default function Home() {




    const dispatch = useDispatch()
    const {articles} = useSelector((state)=>state.articleReducer)



    useEffect(() => {

        dispatch(fetchAllArticle());

    },[])



    return (
        <div className="container">
            <br/>
            <h1>ALL Product Cards</h1>
            <br/>
            <div style={{ display: "flex", flexFlow: "row wrap" }}>
                <br />
                {
                    articles.map((item, index) => (

                        <Card style={{ width: '18rem', margin: "10px" }} key={index}>
                            <Card.Img variant="top" src={item.image} style={{
                                width:"100%",
                                height:"15vw",
                                objectFit:"cover"
                            }}/>
                            <Card.Body>
                                <Card.Title>{item.title}</Card.Title>
                                <Card.Text style={{
                                    overflow: "hidden",
                                    textOverflow:"ellipsis",
                                    display:"-webkit-box",
                                    WebkitLineClamp:"2",
                                    WebkitBoxOrient:"vertical"
                                }}>
                                    {item.description}
                                </Card.Text>
                                <Button variant="primary">Buy</Button>
                            </Card.Body>
                        </Card>

                    ))
                }
            </div>
        </div>
    )
}
